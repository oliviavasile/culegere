package reReHomework;

public class Library {

    public static void main (String[] args) {

        Author author1 = new Author("Vonnegut" , "vonnegut@gmail.com");
        Book book1 = new Book("Leaganul pisicii" , 2019 , author1 , 150);
        Author author2 = new Author("Asimov" , "asimov@gmail.com");
        Book book2 = new Book("Fundatia" , 2020 , author2 , 200);
        book1.printBook();
        book2.printBook();

    }
}
