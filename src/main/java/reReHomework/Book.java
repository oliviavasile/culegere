package reReHomework;

public class Book {
    private String name;
    private int year;
    private Author author;
    private double price;

    public Book(){

    }
    public Book( String name, int year, Author author, double price){
        this.name = name;
        this.year = year;
        this.author = author;
        this.price = price;
    }

    public String getName ( ) {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public int getYear ( ) {
        return year;
    }

    public void setYear (int year) {
        this.year = year;
    }

    public Author getAuthor ( ) {
        return author;
    }

    public void setAuthor (Author author) {
        this.author = author;
    }

    public double getPrice ( ) {
        return price;
    }

    public void setPrice (double price) {
        this.price = price;
    }

    public void printBook() {
        System.out.println("Book " + getName() + "("  + getPrice() + " RON "+ ")" + "by " + getAuthor() + " published in " + getYear());
    }

    @Override
    public String toString ( ) {
        return "Book" +
                "name='" + name + '\'' +
                ", year=" + year +
                ", author=" + author +
                ", price=" + price;
    }
}
