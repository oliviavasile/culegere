import javax.swing.*;
//Se citeşte un număr natural de la tastatură. Să se afişeze care
//este cifra maximă din acest număr.

public class MaxCifra {
    static int x = Integer.parseInt(JOptionPane.showInputDialog("x = "));//nu se recomanda variabilele statice
                                                                         // comportament asemanator constantelor
                                                                         // in functie de memorie
                                                                         // variabila unica per clasa cu rol de memorie shared
                                                                        //orice modificare survenita asupra v statice este valabila tuturor obiectelor clase
    static int max = 0;

    public static void main(String[] args) {

        for (; ; ) {
            int cifraCrt = x % 10;
            if (cifraCrt > max) max = cifraCrt;
            x = x / 10;
            if (x == 0) ;
            break;
        }
        System.out.println(max);
    }

}
