package Homework6;

public class Etaj3 extends Cladire {
    public Etaj3 (int spatiuBirou , int toaleta , int bucatarie , int salaConferinta) {
        super(spatiuBirou , toaleta , bucatarie , salaConferinta);
    }

    @Override
    public String toString ( ) {
        return "Etaj3{" +
                "bucatarie=" + bucatarie +
                ", spatiuBirou=" + spatiuBirou +
                ", toaleta=" + toaleta +
                ", salaConferinta=" + salaConferinta +
                '}';
    }
}
