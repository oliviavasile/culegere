package Homework6;

public class Etaj2 extends Cladire {
    public Etaj2 (int spatiuBirou , int toaleta , int bucatarie , int salaConferinta) {
        super(spatiuBirou , toaleta , bucatarie , salaConferinta);
    }

    @Override
    public String toString ( ) {
        return "Etaj2{" +
                "bucatarie=" + bucatarie +
                ", spatiuBirou=" + spatiuBirou +
                ", toaleta=" + toaleta +
                ", salaConferinta=" + salaConferinta +
                '}';
    }
}