package Homework6;

import java.util.HashMap;
import java.util.Map;

public class Floor
{


    public static void showFloor1() {
        Map<String, Integer> floor1 = new HashMap<String, Integer>();
        floor1.put("officeSpaces" , 1);
        floor1.put("toilets" , 2);
        floor1.put("kitchen" , 1);
        floor1.put("confRooms" , 3);

        for (Object key : floor1.keySet()) {
            System.out.println("Floor1 has " + " " + floor1.get(key) + " " + key );
        }
    }

    public static void showFloor2(){
        Map<String, Integer> floor2 = new HashMap<String, Integer>();
        floor2.put("officeSpaces",2);
        floor2.put("toilets",2);
        floor2.put("kitchen",1);
        floor2.put("confRooms",4);
        for(Object key : floor2.keySet()){
            System.out.println("Floor2 has " + " " + floor2.get(key) + " " + key );
        }
    }

    public static void showFloor3(){
        Map<String, Integer> floor3 = new HashMap<String, Integer>();
        floor3.put("toilets",2);
        floor3.put("confRoom",6);
        for(Object key : floor3.keySet()){
            System.out.println("Floor3 has " + " " + floor3.get(key) + " " + key );
        }


    }

    public static void main (String[] args) {
        System.out.println(" The building has " + Floors.values().length + " floors");
        showFloor1();
        showFloor2();
        showFloor3();

    }

    private static class arrayList<T> {
    }
}

