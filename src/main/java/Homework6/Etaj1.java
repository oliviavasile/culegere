package Homework6;

public class Etaj1 extends Cladire {
    public Etaj1 ( int spatiuBirou, int toaleta, int bucatarie, int salaConferinta){
        super(spatiuBirou, toaleta, bucatarie, salaConferinta);}



    @Override
    public String toString ( ) {
        return "Etaj1{" +
                "bucatarie=" + bucatarie +
                ", spatiuBirou=" + spatiuBirou +
                ", toaleta=" + toaleta +
                ", salaConferinta=" + salaConferinta +
                '}';
    }
}
