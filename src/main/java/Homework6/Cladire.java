package Homework6;

public class Cladire {
    int bucatarie;
    int spatiuBirou;
    int toaleta;
    int salaConferinta;

    public Cladire (){

    }

    public Cladire ( int spatiuBirou, int toaleta, int bucatarie,  int salaConferinta) {
        this.bucatarie = bucatarie;
        this.spatiuBirou = spatiuBirou;
        this.toaleta = toaleta;
        this.salaConferinta = salaConferinta;
    }

    public int getBucatarie ( ) {
        return bucatarie;
    }

    public void setBucatarie (int bucatarie) {
        this.bucatarie = bucatarie;
    }

    public int getSpatiuBirou ( ) {
        return spatiuBirou;
    }

    public void setSpatiuBirou (int spatiuBirou) {
        this.spatiuBirou = spatiuBirou;
    }

    public int getToaleta ( ) {
        return toaleta;
    }

    public void setToaleta (int toaleta) {
        this.toaleta = toaleta;
    }

    public int getSalaConferinta ( ) {
        return salaConferinta;
    }

    public void setSalaConferinta (int salaConferinta) {
        this.salaConferinta = salaConferinta;
    }

    @Override
    public String toString ( ) {
        return "Cladire{" +
                "bucatarie=" + bucatarie +
                ", spatiuBirou=" + spatiuBirou +
                ", toaleta=" + toaleta +
                ", salaConferinta=" + salaConferinta +
                '}';
    }
}
