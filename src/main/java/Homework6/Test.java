package Homework6;
//We want to represent a building using Java objects.
//• A building has floors.
//• A floor has more rooms.
//• Rooms have more types: conference room, office space, kitchen,
//toilet.
//• Each room has furniture and appliances.
//Building structure:
//• 1 Building
//• 3 Floors
//• 1st floor: 1 office space with 20 desks, 2 toilets, 1 kitchen, 3 conference rooms
//(each with 10 seats)
//• 2nd floor: 2 office spaces with 10 desks each, 2 toilets, 1 kitchen, 4 conference
//rooms (each with 8 seats)
//• 3rd floor: 2 toilets, 6 conference rooms (one with 30 seats, one with 20 seats and
//the rest with 10 seats each).
//• Each kitchen has a coffee machine, a water dispenser and a fridge.
//• All conference rooms have a TV, except the larger one that has a Video projector.
//The ones from the 3rd floor also have conference equipment (Telepresence).

public class Test {
    public static void main (String[] args) {
        Etaj1 etaj1 = new Etaj1(1,2,1,3);
        //System.out.println(etaj1.toString());
        Etaj2 etaj2 = new Etaj2(2,2,1,4);
        //System.out.println(etaj2.toString());
        Etaj3 etaj3 = new Etaj3(0,2,0,6);
        //System.out.println(etaj3.toString());
        String newline = System.lineSeparator();
        System.out.println("Cladirea are " + Floors.values().length + " etaje" + newline + etaj1.toString() + newline + etaj2.toString() + newline + etaj3.toString());
    }
}
