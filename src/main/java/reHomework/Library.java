package reHomework;

public class Library {
    public static void main(String[] args) {

        Author x = new Author();
        Author y = new Author();

        x.name = "Vonnegut";
        x.email = "von@von.com";

        y.name = "Asimov";
        y.email = "asi@asi.com";


        Book a = new Book();
        Book b = new Book();

        a.name = "Leaganul pisicii";
        a.year = 2019;
        a.author = x;
        a.price = 20.00;

        b.name = "Fundatia";
        b.year = 2020;
        b.author = y;
        b.price = 15.00;

        System.out.println("Book " + a.name + "(" + a.price + " RON " + ")" + "," + "by" + a.author + "," + "published in year" + a.year);
        System.out.println("Book " + b.name + "(" + b.price + " RON " + ")" + "," + "by" + b.author + "," + "published in year" + b.year);




    }
}
