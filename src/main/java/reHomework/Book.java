package reHomework;

public class Book {
    public String name;
    public Author author;
    public int year;
    public double price;

    public Book ( ) {

    }

    public Book (String name , int year , Author author , double price) {

        this.name = name;
        this.year = year;
        this.author = author;
        this.price = price;
    }

    public String getName ( ) {

        return name;
    }

    public Author getAuthor ( ) {

        return author;
    }

    public double getPrice ( ) {

        return price;
    }

    public int getYear ( ) {

        return year;
    }
}
