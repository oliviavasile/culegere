import javax.swing.*;

public class Fibonacci {
    //Se citeşte un număr natural N. Să se calculeze suma primilor N
    //termeni din şirul lui Fibonacci.

    public static void main(String args[]) {
        int N = Integer.parseInt(JOptionPane.showInputDialog("N="));
        int suma = 0;
        for (int i = 0; i < N; i++)
            suma = suma + fib(i);
        ;
        System.out.println(suma);

    }

    private static int fib(int n) {
        if (n == 0) return 1;
        if (n == 1) return 1;
        return fib(n - 1) + fib(n - 2);

    }

}
