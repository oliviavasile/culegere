import javax.swing.*;

public class Ordonare3 {

    //Se citesc de la tastatură trei numere întregi a, b şi c. Să se
    //ordoneze aceste numere crescătoare, astfel încât a va fi minimul, iar în
    //c va fi maximul. Citirea de la tastatură se va face cu metoda
    //showInputDialog() din clasa JOptionPane.
    public static void main(String[] args) {

        int a = Integer.parseInt(JOptionPane.showInputDialog("a="));
        int b = Integer.parseInt(JOptionPane.showInputDialog("b="));
        int c = Integer.parseInt(JOptionPane.showInputDialog("c="));
        if (a > b && b < c) { // musai && b < c
            // le comutam:
            int aux = a;
            a = b;
            b = aux;
        }
        if (b > c && c > a) { //musai && c > a
            //le comutam:
            int aux = b;
            b = c;
            c = aux;
        }

        if (a > c && b > c) { // musai && b > c
            //le comutam:
            int aux = a;
            a = c;
            c = aux;
        }

        System.out.println(a + " " + b + " " + c);
    }
}
