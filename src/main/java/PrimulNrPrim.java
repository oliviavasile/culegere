public class PrimulNrPrim {
    //Să se calculeze primul număr prim mai mare decât un număr
    //dat ca parametru în linia de comandă.

    public static boolean estePrim (int x) {
        boolean este = true;
        for (int i = 2; i < x / 2; i++)
            if (x % i == 0 ){
                este = false;
                break;}
        return este;
    }

    public static void main(String[] args) {
        int nr = Integer.parseInt( args[0]);
        int nrCrt = nr + 1;
        for (;;) { // loop infinit, se opreste cu break
            if(estePrim(nrCrt))
                break;
            else nrCrt++;
        }
        System.out.println(nrCrt);
    }
}