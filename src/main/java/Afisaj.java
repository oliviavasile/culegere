public class Afisaj {
    //Să se se afiseze toate numerele de patru cifre,
    //împărţite la 67, dau restul 23.

    public static void main(String[] args) {

        int i = 1000;
        for (; i <= 9999; i++)
            if (i % 67 == 23)
                System.out.println(i);
    }
}
