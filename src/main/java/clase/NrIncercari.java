package clase;

import java.util.Random;

public class NrIncercari {
    //Să se afişeze din câte încercări se generează trei numere
    //aleatoare egale, în gama 0..19.
    public static void main(String[] args) {
        final int GAMA = 20;
        Random r = new Random();// se creaza instanta prin constructor
        int contor = 0;
        for (; ;){
            int a = r.nextInt(GAMA);//se genereaza urmatorul nrintreg aleatoriu
            int b = r.nextInt(GAMA);
            int c = r.nextInt(GAMA);
            contor++;
            if ((a == b) && (b == c)) break;// break intrerupe bucla infinita care definita mai sus prin (;;)

        }
        System.out.println(contor);

    }
}
