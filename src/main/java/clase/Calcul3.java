package clase;

import javax.swing.*;

public class Calcul3 {
    //Pentru trei numere întregi a, b şi c, să se calculeze maximul,
    //minimul şi media aritmetică a celor trei numere, folosind o metodă
    //separată ce are ca parametrii trei numere întregi şi care returnează trei
    //rezultate: maximul, minimul şi media aritmetică a celor trei numere.

    static int a = Integer.parseInt(JOptionPane.showInputDialog(" a = "));
    static int b = Integer.parseInt(JOptionPane.showInputDialog(" b = "));
    static int c = Integer.parseInt(JOptionPane.showInputDialog(" c = "));
    static double med = (a + b + c)/3;

    public static void main(String[] args) {
        System.out.println("Maximul este " + max (a,b,c));
        System.out.println("Minimul este " + min (a,b,c));
        System.out.println("Media aritmetica este " + med);
    }
    public static int max(int a,int b,int c){
        int max = a;
        if (b > max) b = max;
        if (c > max) c = max;
        return max;
    }
    public static int min ( int a, int b, int c){
        int min = c;
        if (a < min) a = min;
        if (b < min) b = min;
        return min;
    }

}


