package matrice;

public class Student {
        String name;
        float avg;

        public Student(String name, float avg) {
            this.name = name;
            this.avg  = avg;
        }

        public String toString() {
            return "[" + name + ", " + avg + "]";
        }
}
