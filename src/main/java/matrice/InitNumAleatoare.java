package matrice;

import javax.swing.*;
import java.util.Random;

//Se citeşte de la tastatură un număr natural N; se instanţiază un
//vector de N numere întregi. Să se completeze acest vector cu numere
//aleatoare în gama 0..N-1, cu condiţia ca fiecare număr din această
//gamă să apară o singură dată.
// Algoritm: vom iniţializa vectorul cu numerele 0,1, .., N-1, date în
//această ordine. Apoi, aceste numere iniţiale, le vom comuta, poziţiile
//de comutare fiind generate aleator.
public class InitNumAleatoare {

    public static void main (String args[]) {
        int N;
        String s = JOptionPane.showInputDialog("N=");
        N = Integer.parseInt(s);
        int a[] = new int[N];
        int i;
        //se initializeaza vectorul cu numerele 0,1,...,N-1, in aceasta ordine:
        for (i = 0; i < a.length; i++)
            a[i] = i;
        Random r = new Random();
        //se repeta de N ori:
        for (i = 0; i < N; i++) {
            //se genereaza doua numere aleatoare n1 si n2:
            int n1 = r.nextInt(N);
            int n2 = r.nextInt(N);
            //se comuta variabilele a[n1] si a[n2]:
            int aux = a[n1];
            a[n1] = a[n2];
            a[n2] = aux;
        }
        //Afisare vector generat:
        for (i = 0; i < N; i++)
            System.out.println(a[i]);
        System.exit(0);

    }
}