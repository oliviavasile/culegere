package matrice;

import java.util.HashMap;
import java.util.Map;

public class Test {

        public static void main(String[] args) {

            Map<String,Student> students = new HashMap<String, Student>();

            students.put("Matei",  new Student("Matei",  4.90F));
            students.put("Andrei", new Student("Andrei", 6.80F));
            students.put("Mihai",  new Student("Mihai",  9.90F));

            System.out.println(students.get("Mihai"));

            // adaugăm un element cu aceeași cheie
            System.out.println(students.put("Andrei", new Student("", 0.0F)));
            // put(...) întoarce elementul vechi

            // si îl suprascrie
            System.out.println(students.get("Andrei"));

            // remove(...) returnează elementul șters
            System.out.println(students.remove("Matei"));

            // afișăm structura de date
            System.out.println(students);
        }
    }

