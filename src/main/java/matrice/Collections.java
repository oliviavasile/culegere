package matrice;

import constructori.Triangle;

import java.awt.*;
import java.util.*;
import java.util.List;

public class Collections {

    public static void showArrays ( ) {
        int[] arrays = {1 , 3 , 4 , 5};
        for (int el : arrays) {
            System.out.println(el);
        }


        String[] names = {"Ion" , "Maria" , "Vasile"};
        for (String name : names) {
            System.out.println(name);
        }
//int [] numbers = new int[100]; ineficient ceea ce a dus la interfata list si metodele arrayList si Set
    }

    public static void displayList (List list) {
        for (Object lst : list) {
            System.out.println(lst.toString());
        }
    }

    public static void showList ( ) {
        List arrayList = new ArrayList();
        arrayList.add("Ana");
        arrayList.add(1);
        arrayList.add("Ana are mere");
        arrayList.add(0 , 2);

        displayList(arrayList);
        arrayList.remove("Ana");
        displayList(arrayList);

        List<String> names = new ArrayList<String>();
        names.add("Ion");
        names.add("Vasile");
        names.add("Vasile");
        System.out.println(names.toString());
        System.out.println("Size of names" + names.size() + names.toString());
        System.out.println(names.get(1));

    }

    // set lista cu elemente unice
    public static void showSet ( ) {
        Set<Integer> setList = new HashSet<Integer>();
        setList.add(1);
        setList.add(2);
        setList.add(3);
        setList.add(3);
        System.out.println(setList.toString());

    }

    public static void displayMap(Map hashMap){
        for(Object key : hashMap.keySet()){
            System.out.println(key + "" + hashMap.get(key));
        }
    }
 public static void showMap(){
     Map<String, Integer> hashMap = new HashMap<String, Integer>();
     hashMap.put("laptop",3);
     hashMap.put("pc",5);
     hashMap.put("pc",2);
     hashMap.put("mobile",4);
     displayMap(hashMap);
 }

    public static void main (String[] args) {
        //showArrays();
        //showList();}
//        showSet();
      showMap();
  }
}
