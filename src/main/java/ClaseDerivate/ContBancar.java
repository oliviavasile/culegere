package ClaseDerivate;
//Să se construiască clasa ContBancar, folosită pentru a modela
//un cont bancar, ce are ca variabilă de instanţă privată, variabila suma,
//(suma de bani din cont). Ca metode:
//‐ constructorul;
//‐ adauga(), ce are ca parametru un număr real x, valoarea ce se
//adaugă în cont;
//‐ extrage(), ce are ca parametru un număr real x, valoarea ce se
//extrage din cont, şi care scoate ca rezultat true, dacă se poate face
//extragerea (suma >= x), şi false în caz contrar;
//‐ getSuma(), ce returnează valoarea variabilei private suma;
//‐ afisare(), ce afişează valoarea sumei de bani din cont.
//Pe baza clasei ContBancar se va dezvolta prin derivare (moştenire)
//clasa ContBancarExtins, în care se va adăuga o nouă variabilă de
//instanţă: rata dobânzii anuale şi o nouă metodă:
//adaugaDobandaLunara(), ce adaugă în cont dobânda calculată după
//trecerea unei luni. În clasa ContBancarExtins se va redefini şi metoda
//afisare(), astfel încât să se afişeze şi rata dobânzii. De asemenea, în
//această nouă clasă se va defini constructorul, prin care se iniţializează
//suma de bani din cont şi rata dobânzii.
//Să se scrie şi o clasă de test pentru clasa ContBancarExtins.

public class ContBancar {

    private double suma;

    public ContBancar(double S) {
        suma = S;
    }

    public double getSuma ( ) {
        return suma;
    }

    public void setSuma (double suma) {
        this.suma = suma;
    }
    public void adauga(double S){
        suma = suma + S;

    }
    public boolean extrage(double S){
        if (S >= suma) return true;
        suma = suma - S;
        return true;
    }
    public void afisare(){
        System.out.println(" suma = " + suma);

    }
}
