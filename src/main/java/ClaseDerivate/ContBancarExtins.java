package ClaseDerivate;

public class ContBancarExtins extends ContBancar{

    private double rda;

public ContBancarExtins (double suma){
    super(suma);
        }

    public ContBancarExtins(double suma, double rda){
        super(suma);
        this.rda = rda;

        }
        public void adaugaDobandaLunara() {
    double suma = this.getSuma();
    double dobanda = suma * rda/12;
    this.adauga(dobanda);
        }

    public void afisare() {
        System.out.println("suma=" + this.getSuma());
        System.out.println("rata dobanzii=" + rda);
    }


}
