public class Scadere {
    //Scrieţi o aplicaţie Java care să reconstituie scăderea:
    //X X X –
    //X 2 X
    //---------
    //8 7 9

    public static void main(String args[]) {

        int s1;
        int z1;
        int u1;
        int s2;
        int u2;

        for (u1 = 0; u1 <= 9; u1++)
            for (z1 = 0; z1 <= 9; z1++)
                for (s1 = 1; s1 <= 9; s1++)
                    for (u2 = 0; u2 <= 9; u2++)
                        for (s2 = 1; s2 <= 9; s2++) {
                            int no1 = 100 * s1 + 10 * z1 + u1;
                            int no2 = 100 * s2 + 20 + u2;
                            if ( no1 - no2 == 879) {
                                System.out.println(no1 + " - " + no2 + " = " + "798");
                            }
                        }
    }
}
