package encapsulation;

import java.awt.*;

public class MainClassE {
    public static void main (String[] args) {
        Car logan = new Car (Color.blue, (byte)5, 150);

        logan.setGear((byte)1);
        System.out.println(logan.getGear());
        logan.start();
        System.out.println(logan.getColorOfCar());
        System.out.println(logan.toString());
        logan.accelerate();
        logan.accelerate();
        System.out.println(logan.toString());

    }
}
