package constructori;

import java.awt.*;

public class Car {
    private float fuelLevel;
    private byte gear;
    private byte maxGear;
    private float speed;
    private float maxSpeed;
    private Color color;

    private boolean engineRunning;

    public Car (Color color , byte maxGear , float maxSpeed) {
        this.color = color;
        this.maxGear = maxGear;
        this.maxSpeed = maxSpeed;
    }

    public void accelerateSpeed (float speedDelta) {
        speed = speed + speedDelta;
        if( speed > maxSpeed)
            speed = maxSpeed;

    }
    public void accelerate (){
        accelerateSpeed(10);
        if ( speed > 0 && speed <= 20)
            this.gear = 1;
        else if (speed >= 20 && speed <= 50)
            this.gear = 2;
        else if ( speed >= 51 && speed <= 101)
                this.gear =3;
        else if (speed == 0)
            this.gear = 0;
    }

    public void steer (float enagle) {

    }

    public void gearUP ( ) {
        if(gear < maxGear)
        this.gear++;

    }

    public void gearDown ( ) {
        if( gear > -1)
            this.gear--;
    }

    public void stop ( ) {
        setEngineRunning(false);
        speed = 0;

    }

    public void start ( ) {
        this.gear = 1;
        accelerateSpeed(20);
        setEngineRunning(true);
    }

    public static void horn ( ) {
        System.out.println(" tit tit tit...");
    }

    public float getFuelLevel ( ) {
        return fuelLevel;
    }

    public void setFuelLevel (float fuelLevel) {
        this.fuelLevel = fuelLevel;
    }

    public byte getGear ( ) {
        return gear;
    }

    public void setGear (byte gear) {
        this.gear = gear;
    }

    public byte getMaxGear ( ) {
        return maxGear;
    }

    public void setMaxGear (byte maxGear) {
        this.maxGear = maxGear;
    }

    public float getSpeed ( ) {
        return speed;
    }

    public void setSpeed (float speed) {
        this.speed = speed;
    }

    public float getMaxSpeed ( ) {
        return maxSpeed;
    }

    public void setMaxSpeed (float maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public Color getColor ( ) {
        return color;
    }

    public void setColor (Color color) {
        this.color = color;
    }

    public boolean isEngineRunning ( ) {
        return engineRunning;
    }

    public void setEngineRunning (boolean engineRunning) {
        this.engineRunning = engineRunning;
    }
    public String getColorOfCar() {

        if(color.equals(Color.RED))
            return "RED";
        else if(color.equals(Color.BLUE))
            return "BLUE";
        else if(color.equals(Color.YELLOW))
            return "YELLOW";
        else if(color.equals(Color.GREEN))
            return "GREEN";
        else if (color.equals(Color.ORANGE))
            return "ORANGE";
        else
            return "Unknown Color";
    }

    @Override
    public String toString ( ) {
        return "Car{" +
                "fuelLevel=" + fuelLevel +
                ", gear=" + gear +
                ", maxGear=" + maxGear +
                ", speed=" + speed +
                ", maxSpeed=" + maxSpeed +
                ", color=" + getColorOfCar() +
                ", engineRunning=" + engineRunning +
                '}';
    }
}


