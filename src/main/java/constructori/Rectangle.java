package constructori;

public class Rectangle {

    private double length;// or private double length, width
    private double width;

    public double getArea() {

        return length * width;
    }

    public double getPerimeter(){

        return 2 * (length + width);
    }

    public double getDiagonal(){

        return Math.sqrt(Math.pow(length,2) + Math.pow (width,2));
    }
public Rectangle (double length, double width) {
      this.length = length;
      this.width = width;
}
    public Rectangle () {
}
}
