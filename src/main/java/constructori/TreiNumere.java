package constructori;
//Să se dezvolte clasa Pitagorice, ce are variabile de instanţă
//trei numere întregi a,b şi c, şi ca metode:
//‐ constructorul ce face iniţializările;
//‐ metoda maxim(), ce returnează maximul dintre a, b şi c;
//‐ metoda suntPitagorice(), ce returnează true, dacă a, b, c sunt
//numere pitagorice.
//Scrieţi şi o clasă de test pentru clasa TreiNumere.
public class TreiNumere {

int a;
int b;
int c;

public TreiNumere(int a, int b, int c) {
    this.a = a;
    this.b = b;
    this.c = c;

}
public int maxim() {
    int max = a;
    if(b > max)max = b;
    if(c > max) max = c;
    return max;
};




}