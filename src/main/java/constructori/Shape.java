package constructori;

import java.awt.*;

public class Shape {
    private Color color;

    public Shape(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    protected void draw() {
        System.out.println("Draw a shape with color:" + color.toString());
    }

    public void erase() {
        System.out.println("Erase the shape with color:" + color.toString());
    }

}
