package constructori;

import encapsulation.Car;

import java.awt.*;

public class MainClass {
    public static void main (String[] args) {
        encapsulation.Car logan = new Car(Color.blue, (byte)5, 150);

        logan.setGear((byte)1);
        System.out.println(logan.getGear());
        logan.start();
        System.out.println(logan.getColorOfCar());
        System.out.println(logan.toString());
        logan.accelerate();
        logan.accelerate();
        System.out.println(logan.toString());

        Triangle tr = new Triangle(Color.black);
        tr.draw();
        tr.erase();

        Shape shapeTr = new Triangle(Color.green);
        Shape shape = new Shape(Color.RED);

        //-----------------------------------
        System.out.println("-----------------Inheritance Exercise--------");
        PersonC5 person1= new PersonC5("Ion Vasile", "Bucuresti", "1231223123123123", 45, "M");
        Teacher teacher1 = new Teacher("Popescu Vasilica", "Bucuresti", "123112233123123123", 60, "F", "Math", 3456);
        System.out.println(person1.toString());
        System.out.println(teacher1.toString());


        Student student1 = new Student("Popescu Ana", "Bucuresti", "2805246789745", 20, "F", 5f, 2, 6f);
        System.out.println(student1.toString());

    }
}
