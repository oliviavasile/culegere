package constructori;

public class Exercices {

    public static void main(String[] args) {

        Person p1 = new Person();// instantierea unui obiect
        Person p2 = new Person ();//p1,p2 si -a insusit atributele si actiunile pe care le poate executa
        //toate obiectele din clasa person
        p1.name = "Alex";
        p1.age = 33;
        p1.isHungry = false;

        p2.name = "Diana";
        p2.age =18;
        p2.isHungry = true;

        System.out.println(p1.name + "si" + p2.name + "participa la cursul de automation");
        p1.printPerson();
        p2.printPerson();

        Circle circle1 = new Circle();
        circle1.printCircle();
        circle1.setRadius(2f);
        circle1.printCircle();
        System.out.println(circle1.radius);


        System.out.println(circle1.getArea());

        Circle c2 = new Circle();
        c2.setRadius(9f);
        System.out.println(c2.getArea());

        TreiNumere t = new TreiNumere(1, 5, 18);

        System.out.println("maximul este: " + t.maxim());
    }
}
