package constructori;

public class Student extends PersonC5 {

    private float grade;
    private int noOfGrade;
    private float finalGrade;

    public Student(String name , String address , String cnp , int age , String gender){
        super (name, address, cnp, age, gender);
    }
    public Student (String name , String address , String cnp , int age , String gender, float grade, int noOfGrade, float finalGrade ){
        super(name, address, cnp, age, gender);
        this.grade = grade;
        this.noOfGrade = noOfGrade;
        this.finalGrade = finalGrade;
    }

    public float getGrade ( ) {
        return grade;
    }

    public void setGrade (float grade) {
        this.grade = grade;
    }

    public int getNoOfGrade ( ) {
        return noOfGrade;
    }

    public void setNoOfGrade (int noOfGrade) {
        this.noOfGrade = noOfGrade;
    }

    public float getFinalGrade ( ) {
        return finalGrade;
    }

    public void setFinalGrade (float finalGrade) {
        this.finalGrade = finalGrade;
    }

    @Override
    public String toString ( ) {
        return super.toString()  +
                "grade=" + grade +
                "noOfGrade = " + noOfGrade +
                "finalGrade = " + finalGrade +
                '}';
    }
}
