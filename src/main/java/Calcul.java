public class Calcul {
    //Să se calculeze şi afişeze câte numere naturale de patru cifre,
    //împărţite la 67, dau restul 23.
    public static void main(String[] args) {
        int contor = 0;
        int i = 1000;
        for (; i < 10000; i++)// daca nu initializam i inainte de for vom avea for ( i = 1000; i < 10000; i++ )
            if ( i%67 == 23) contor++;
        System.out.println(contor);
    }
}
